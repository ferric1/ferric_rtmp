use crate::connection::{ObservableConnectionState};
use std::sync::{Arc, Mutex};

pub mod impl_server;
mod server;

/// The `Server` struct represents a server in the application.
///
/// # Fields
///
/// * `address` - A string that holds the IP address and port number of the server.
/// * `connections` - A vector of `Arc<Mutex<ObservableConnectionState>>` objects that represent the active connections to the server. Each `ObservableConnectionState` is wrapped in a `Mutex` for thread safety, and the `Mutex` is wrapped in an `Arc` (Atomic Reference Counting) for shared ownership across multiple threads.
#[derive(Debug)]
pub struct Server {
    address: String,
    connections: Vec<Arc<Mutex<ObservableConnectionState>>>,
}
