use crate::connection;
use crate::server::Server;
use log::{error, info};
use std::sync::{Arc, Mutex};
use tokio::net::TcpListener;
use tokio::net::TcpStream;
use tokio::task;
//use anyhow::Result;

// Impl for Server struct
impl Server {
    /// Constructs a new `Server` instance.
    ///
    /// # Arguments
    ///
    /// * `addr` - A string that represents the IP address and port number of the server.
    ///
    /// # Returns
    ///
    /// * `Self` - Returns a new `Server` instance with the specified address and an empty vector for connections.
    pub fn new(addr: String) -> Self {
        Self {
            address: addr,
            connections: Vec::new(),
        }
    }

    /// This function runs the server.
    ///
    /// # Arguments
    ///
    /// * `server` - An `Arc<Mutex<Server>>` instance representing the server.
    ///
    /// # Functionality
    ///
    /// This function does the following:
    /// 1. Locks the server and binds a `TcpListener` to the server's address.
    /// 2. Enter a loop where it accepts incoming connections.
    /// 3. For each accepted connection, it spawns a new task to process the connection.
    ///
    /// # Returns
    ///
    /// * `Ok(())` - If the server runs successfully, it returns an empty Ok result.
    /// * `Err(Box<dyn std::error::Error>)` - If any part of the server run fails, it returns an error.
    pub async fn run(server: Arc<Mutex<Self>>) -> Result<(), Box<dyn std::error::Error>> {
        // Create a TcpListener and bind it to the server's address
        let listener = {
            let locked_server = server.lock().unwrap();
            match TcpListener::bind(&locked_server.address).await {
                Ok(listener) => {
                    info!("Listening on: {}", locked_server.address);
                    listener
                }
                Err(e) => {
                    error!("Failed to bind to address: {}", e);
                    return Err(e.into());
                }
            }
        };

        // Enter a loop to accept incoming connections
        loop {
            // Accept a new connection
            let (stream, addr) = match listener.accept().await {
                Ok((stream, addr)) => {
                    info!("New Connection: {}", addr);
                    (stream, addr)
                }
                Err(e) => {
                    error!("Failed to accept connection: {}", e);
                    continue;
                }
            };

            // Clone the server and spawn a new task to process the connection
            let server_clone = Arc::clone(&server);
            task::spawn(async move {
                if let Err(e) = Server::process(server_clone, stream).await {
                    error!("Failed to process connection: {}", e);
                }
            });
        }
    }

    /// This function processes a new connection to the server.
    ///
    /// # Arguments
    ///
    /// * `server` - An `Arc<Mutex<Server>>` instance representing the server.
    /// * `stream` - A `TcpStream` instance representing the new connection.
    ///
    /// # Functionality
    ///
    /// This function does the following:
    /// 1. Creates a new `Connection` instance from the `TcpStream`.
    /// 2. Gets a reference to the observable state of the connection.
    /// 3. Locks the server and adds the observable state of the new connection to the server's connections.
    /// 4. Runs the new connection.
    ///
    /// # Returns
    ///
    /// * `Ok(())` - If the connection is processed successfully, it returns an empty Ok result.
    /// * `Err(Box<dyn std::error::Error>)` - If any part of the connection processing fails, it logs an error message and returns an error.
    async fn process(
        mut server: Arc<Mutex<Server>>,
        stream: TcpStream,
    ) -> Result<(), Box<dyn std::error::Error>> {
        // Create a new connection
        let mut connection = connection::Connection::new(stream);

        // Get a reference to the observable state of the connection
        let state = &connection.observable_state;

        // Lock the server and add the observable state of the new connection to the server's connections
        {
            let mut locked_server = server.lock().unwrap();
            locked_server.connections.push(Arc::clone(&state));
        }

        // Now, run the connection outside of the locked block
        match connection.run().await {
            Ok(_) => Ok(()),
            Err(e) => {
                error!("Failed to run connection: {}", e);
                Err(e.into())
            }
        }
    }

    pub fn observe_connections(&self) -> Result<(), Box<dyn std::error::Error + '_>>  {
        self.connections.iter().map(|state_arc| Ok({
            // Print the state of the connection
            let state = match state_arc.lock() {
                Ok(state) => state,
                Err(e) => {
                    error!("Failed to lock connection state: {}", e);
                    return Err(e.into());
                }
            };
            println!("Connection {:?}", state);
        })).collect()
    }
}
