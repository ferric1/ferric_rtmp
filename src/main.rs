mod amf0;
mod connection;
mod server;

use crate::server::Server;
use flexi_logger::{FileSpec, Logger, WriteMode};
use log::info;
use std::sync::{Arc, Mutex};

/// This is the main function of the application.
///
/// # Functionality
///
/// This function does the following:
/// 1. Initializes the logger with the specified configuration.
/// 2. Defines the IP and port for the server.
/// 3. Creates a new instance of the `Server` struct with the specified address.
/// 4. Wraps the server instance in an `Arc<Mutex<>>` for thread safety.
/// 5. Calls the `run` method on the `Server` struct, passing in the wrapped server instance.
///
/// # Returns
///
/// * `Ok(())` - If the server starts successfully, it returns an empty Ok result.
/// * `Err(Box<dyn std::error::Error>)` - If any part of the server initialization or run fails, it returns an error.
#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Initialize the logger
    let _logger = Logger::try_with_str("debug, my::critical::module=trace")?
        .log_to_file(FileSpec::default().directory("logs").use_timestamp(false))
        .write_mode(WriteMode::BufferAndFlush)
        .start()?;

    // Define the IP and port for the server
    let ip = "0.0.0.0";
    let port = "1935";
    let addr = format!("{}:{}", ip, port);

    // Create a new instance of the Server struct
    let server = Server::new(addr.to_owned());
    println!("Starting server on {}", addr);
    info!("Starting server");

    // Wrap the server in an Arc<Mutex<>> for thread safety
    let server_arc = Arc::new(Mutex::new(server));

    // Call the run method on the Server struct, passing in the wrapped server instance
    Server::run(server_arc).await?;

    Ok(())
}
