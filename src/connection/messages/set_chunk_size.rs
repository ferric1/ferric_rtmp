#[derive(Debug, Clone)]
pub struct SetChunkSizeMessage {
    pub chunk_size: usize,
}

impl SetChunkSizeMessage {
    pub fn new(chunk_size: usize) -> SetChunkSizeMessage {
        SetChunkSizeMessage { chunk_size }
    }
}