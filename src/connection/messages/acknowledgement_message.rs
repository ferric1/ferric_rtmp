#[derive(Debug)]
pub struct AcknowledgementMessage {
    pub sequence_number: usize,
}

impl AcknowledgementMessage {
    pub fn new(sequence_number: usize) -> AcknowledgementMessage {
        AcknowledgementMessage { sequence_number }
    }
}