use bytes::BytesMut;
use indexmap::IndexMap;
use crate::amf0::define::Amf0ValueType;
use crate::amf0::errors::Amf0WriteError;
use crate::amf0::writer::Amf0Writer;
use crate::connection::messages::CommandObject;

/// The `ResultObject` struct represents a result object in the application.
///
/// # Fields
///
/// * `command_name` - A string that represents the name of the command associated with the result object.
/// * `transaction_id` - A unique identifier for the transaction associated with the result object.
/// * `object` - An optional `CommandObject` that represents the command object associated with the result object.
/// * `stream_id` - A unique identifier for the stream associated with the result object.
pub struct ResultObject{
    pub command_name: String,
    pub transaction_id: usize,
    pub object: Option<CommandObject>,
    pub stream_id: usize,
}

impl ResultObject {
    /// Constructs a new `ResultObject` instance.
    ///
    /// # Arguments
    ///
    /// * `command_name` - A string that represents the name of the command associated with the result object.
    /// * `transaction_id` - A unique identifier for the transaction associated with the result object.
    /// * `stream_id` - A unique identifier for the stream associated with the result object.
    ///
    /// # Returns
    ///
    /// * `ResultObject` - Returns a new `ResultObject` instance with the specified parameters.
    pub fn new(command_name: String, transaction_id: usize, stream_id: usize) -> ResultObject {
        ResultObject {
            command_name,
            transaction_id,
            object: None,
            stream_id,
        }
    }

    /// Sets the command object of the `ResultObject` instance.
    ///
    /// # Arguments
    ///
    /// * `command_object` - A `CommandObject` that represents the command object to be set.
    pub fn set_command_object(&mut self, command_object: CommandObject) {
        self.object = Some(command_object);
    }

    /// Parses the `ResultObject` instance into a byte sequence.
    ///
    /// # Functionality
    ///
    /// This function does the following:
    /// 1. Creates a new `Amf0Writer` instance.
    /// 2. Writes the command name, transaction ID, and stream ID to the `Amf0Writer` instance.
    /// 3. If the `ResultObject` instance has a command object, it writes the properties of the command object to the `Amf0Writer` instance.
    /// 4. Extracts the bytes from the `Amf0Writer` instance.
    ///
    /// # Returns
    ///
    /// * `Ok(BytesMut)` - If the parse is successful, it returns a `BytesMut` with the bytes of the `ResultObject` instance.
    /// * `Err(Amf0WriteError)` - If any part of the parse fails, it returns an error.
    pub fn parse(&self) -> Result<BytesMut, Amf0WriteError> {
        let mut writer = Amf0Writer::new(bytesio::bytes_writer::BytesWriter::new());
        writer
            .write_any(&Amf0ValueType::UTF8String(self.command_name.clone()))
            .unwrap();
        writer
            .write_any(&Amf0ValueType::Number(self.transaction_id as f64))
            .unwrap();
        if Option::is_some(&self.object) {
            let mut command_obj_map = IndexMap::new();

            command_obj_map.insert(
                "fmsVer".to_string(),
                Amf0ValueType::UTF8String(self.object.as_ref().unwrap().fms_ver.clone()),
            );
            command_obj_map.insert(
                "capabilities".to_string(),
                Amf0ValueType::Number(self.object.as_ref().unwrap().capabilities as f64),
            );

            writer
                .write_any(&Amf0ValueType::Object(command_obj_map))
                .unwrap();
        } else {
            writer.write_any(&Amf0ValueType::Null).unwrap();
        }
        writer
            .write_any(&Amf0ValueType::Number(self.stream_id as f64))
            .unwrap();
        let tmp = writer.extract_current_bytes();
        Ok(tmp)
    }
}