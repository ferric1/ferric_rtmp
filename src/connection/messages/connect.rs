use bytes::BytesMut;
use bytesio::bytes_reader::BytesReader;
use log::error;
use crate::amf0::define::Amf0ValueType;
use crate::amf0::reader::Amf0Reader;

/// The `ConnectMessage` struct represents a connection message in the application.
///
/// # Fields
///
/// * `id` - A unique identifier for the connection message.
/// * `app` - A string that represents the application associated with the connection message.
/// * `flash_ver` - A string that represents the version of Flash used by the client.
/// * `swf_url` - A string that represents the URL of the SWF file used by the client.
/// * `tc_url` - A string that represents the URL of the target container for the connection.
/// * `stream_type` - A string that represents the type of the stream for the connection.
#[derive(Default, Debug, Clone)]
pub struct ConnectMessage {
    pub id: usize,
    pub app: String,
    pub flash_ver: String,
    pub swf_url: String,
    pub tc_url: String,
    pub stream_type: String,
}

impl ConnectMessage {
    /// Constructs a new `ConnectMessage` instance.
    ///
    /// # Arguments
    ///
    /// * `id` - A unique identifier for the connection message.
    /// * `app` - A string that represents the application associated with the connection message.
    /// * `flash_ver` - A string that represents the version of Flash used by the client.
    /// * `swf_url` - A string that represents the URL of the SWF file used by the client.
    /// * `tc_url` - A string that represents the URL of the target container for the connection.
    /// * `stream_type` - A string that represents the type of the stream for the connection.
    ///
    /// # Returns
    ///
    /// * `ConnectMessage` - Returns a new `ConnectMessage` instance with the specified parameters.
    pub fn new(id: usize, app: String, flash_ver: String, swf_url: String, tc_url: String, stream_type: String) -> ConnectMessage {
        ConnectMessage {
            id,
            app,
            flash_ver,
            swf_url,
            tc_url,
            stream_type,
        }
    }

    /// Constructs a default `ConnectMessage` instance.
    ///
    /// # Returns
    ///
    /// * `ConnectMessage` - Returns a new `ConnectMessage` instance with default parameters.
    pub fn default() -> ConnectMessage {
        ConnectMessage {
            id: 0,
            app: String::new(),
            flash_ver: String::new(),
            swf_url: String::new(),
            tc_url: String::new(),
            stream_type: String::new(),
        }
    }

    /// Parses a `ConnectMessage` from a byte slice.
    ///
    /// # Arguments
    ///
    /// * `data` - A byte slice that represents the data to be parsed.
    ///
    /// # Functionality
    ///
    /// This function does the following:
    /// 1. Creates a default `ConnectMessage` instance.
    /// 2. Creates a new `Amf0Reader` instance with the data.
    /// 3. Reads all AMF0 values from the data.
    /// 4. Sets the id of the `ConnectMessage` instance to the transaction ID from the decoded message.
    /// 5. Iterates over the decoded object and sets the corresponding fields of the `ConnectMessage` instance.
    ///
    /// # Returns
    ///
    /// * `Ok(ConnectMessage)` - If the `ConnectMessage` is parsed successfully, it returns the `ConnectMessage` instance.
    /// * `Err(Box<dyn std::error::Error>)` - If any part of the parsing fails, it returns an error.
    pub fn parse(data: &[u8]) -> Result<ConnectMessage, Box<dyn std::error::Error>> {
        let mut connect_msg = ConnectMessage::default();
        let mut reader = Amf0Reader::new(BytesReader::new(BytesMut::from(data)));

        let decoded_msg = match reader.read_all() {
            Ok(msg) => msg,
            Err(e) => return Err(Box::try_from(e).unwrap()),
        };

        connect_msg.id = match decoded_msg.get(1) {
            Some(&Amf0ValueType::Number(num)) => num as usize,
            _ => {
                error!(
                    "Failed to get transaction ID from decoded message: {:?}",
                    decoded_msg
                );
                return Err(Box::new(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Expected transaction ID",
                )))
            }
        };

        let decoded_obj = match decoded_msg.get(2) {
            Some(Amf0ValueType::Object(obj)) => obj,
            _ => {
                error!(
                    "Failed to get command object from decoded message: {:?}",
                    decoded_msg
                );
                return Err(Box::new(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Expected command object",
                )));
            }
        };

        for (key, value) in decoded_obj {
            match key.as_str() {
                "app" => {
                    if let Amf0ValueType::UTF8String(s) = value {
                        connect_msg.app = s.clone();
                    }
                }
                "flashVer" => {
                    if let Amf0ValueType::UTF8String(s) = value {
                        connect_msg.flash_ver = s.clone();
                    }
                }
                "tcUrl" => {
                    if let Amf0ValueType::UTF8String(s) = value {
                        connect_msg.tc_url = s.clone();
                    }
                }
                "swfUrl" => {
                    if let Amf0ValueType::UTF8String(s) = value {
                        connect_msg.swf_url = s.clone();
                    }
                }
                "type" => {
                    if let Amf0ValueType::UTF8String(s) = value {
                        connect_msg.stream_type = s.clone();
                    }
                }
                _ => {
                    error!("Unexpected key in decoded object: {}", key);
                }
            }
        }

        Ok(connect_msg)

    }
}