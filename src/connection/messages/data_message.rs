#[derive(Debug)]
pub struct DataMessage {
    id: usize,
    pub data: Vec<u8>,
}

impl DataMessage {
    pub fn new(id: usize, data: Vec<u8>) -> DataMessage {
        DataMessage { id, data }
    }
}