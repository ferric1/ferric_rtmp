use log::info;

/// The `ChunkBasicHeader` struct represents the basic header of a chunk in the RTMP protocol.
///
/// # Fields
///
/// * `fmt` - A `ChunkFmt` enum that represents the format of the chunk.
/// * `cs` - A byte that represents the chunk stream ID.
#[derive(Debug)]
pub struct ChunkBasicHeader {
    pub(crate) fmt: ChunkFmt,
    pub(crate) cs: u8,
}

impl ChunkBasicHeader {
    /// Constructs a new `ChunkBasicHeader` instance.
    ///
    /// # Arguments
    ///
    /// * `byte` - A reference to a byte that represents the first byte of the chunk header.
    ///
    /// # Functionality
    ///
    /// This function does the following:
    /// 1. Splits the byte into the chunk stream ID and the format.
    /// 2. Constructs a new `ChunkBasicHeader` instance with the specified chunk stream ID and format.
    ///
    /// # Returns
    ///
    /// * `Self` - Returns a new `ChunkBasicHeader` instance with the specified chunk stream ID and format.
    pub(crate) fn new(byte: &u8) -> ChunkBasicHeader {
        // split into the chunk header and the message body
        let cs = byte & 0b_00111111;
        let fmt = match ChunkFmt::from_u8((byte >> 6) & 0b_00000011) {
            Some(fmt) => fmt,
            None => panic!("Invalid chunk format"),
        };

        ChunkBasicHeader { fmt, cs }
    }
}

/// The `ChunkFmt` enum represents the format of a chunk in the RTMP protocol.
///
/// # Variants
///
/// * `Type0` - Represents the type 0 format.
/// * `Type1` - Represents the type 1 format.
/// * `Type2` - Represents the type 2 format.
/// * `Type3` - Represents the type 3 format.
#[derive(Debug)]
pub enum ChunkFmt {
    Type0,
    Type1,
    Type2,
    Type3,
}

impl ChunkFmt {
    /// Constructs a new `ChunkFmt` instance from a u8 value.
    ///
    /// # Arguments
    ///
    /// * `value` - A u8 value that represents the format of the chunk.
    ///
    /// # Functionality
    ///
    /// This function does the following:
    /// 1. Matches the value to the corresponding format type.
    /// 2. If the value matches a format type, it returns a `Some` variant of the `Option` enum with the corresponding `ChunkFmt` variant.
    /// 3. If the value does not match any format type, it returns a `None` variant of the `Option` enum.
    ///
    /// # Returns
    ///
    /// * `Option<Self>` - Returns an `Option` enum with a `ChunkFmt` variant if the value matches a format type, or `None` if it does not.
    pub(crate) fn from_u8(value: u8) -> Option<Self> {
        match value {
            0 => Some(Self::Type0),
            1 => Some(Self::Type1),
            2 => Some(Self::Type2),
            3 => Some(Self::Type3),
            _ => None,
        }
    }
}

/// The `ChunkMessageHeader` struct represents the header of a chunk in the RTMP protocol.
///
/// # Fields
///
/// * `timestamp` - An optional 32-bit integer that represents the timestamp of the chunk.
/// * `timestamp_delta` - An optional 32-bit integer that represents the timestamp delta of the chunk.
/// * `message_length` - An optional 32-bit integer that represents the length of the message.
/// * `message_type_id` - An optional byte that represents the type ID of the message.
/// * `message_stream_id` - An optional 32-bit integer that represents the ID of the stream that the message belongs to.
pub struct ChunkMessageHeader {
    timestamp: Option<u32>,
    timestamp_delta: Option<u32>,
    pub(crate) message_length: Option<u32>,
    pub(crate) message_type_id: Option<u8>,
    pub(crate) message_stream_id: Option<u32>,
}

impl ChunkMessageHeader {
    /// Constructs a new `ChunkMessageHeader` instance with default values.
    ///
    /// # Returns
    ///
    /// * `ChunkMessageHeader` - Returns a new `ChunkMessageHeader` instance with all fields set to `None`.
    fn default() -> ChunkMessageHeader {
        ChunkMessageHeader {
            timestamp: None,
            timestamp_delta: None,
            message_length: None,
            message_type_id: None,
            message_stream_id: None,
        }
    }

    /// Constructs a new `ChunkMessageHeader` instance from a byte slice representing a type 0 chunk.
    ///
    /// # Arguments
    ///
    /// * `bytes` - A byte slice that represents the data to be parsed.
    ///
    /// # Returns
    ///
    /// * `ChunkMessageHeader` - Returns a new `ChunkMessageHeader` instance with the parsed values.
    pub(crate) fn type0(bytes: &[u8]) -> ChunkMessageHeader {
        let mut chunk_message_header = ChunkMessageHeader::default();

        let timestamp = u32::from_be_bytes([0, bytes[0], bytes[1], bytes[2]]);
        let message_length = u32::from_be_bytes([0, bytes[3], bytes[4], bytes[5]]);
        let message_type_id = bytes[6];
        let message_stream_id = u32::from_be_bytes([bytes[10], bytes[9], bytes[8], bytes[7]]);

        chunk_message_header.timestamp = Some(timestamp);
        chunk_message_header.message_length = Some(message_length);
        chunk_message_header.message_type_id = Some(message_type_id);
        chunk_message_header.message_stream_id = Some(message_stream_id);

        chunk_message_header
    }

    /// Constructs a new `ChunkMessageHeader` instance from a byte slice representing a type 1 chunk.
    ///
    /// # Arguments
    ///
    /// * `bytes` - A byte slice that represents the data to be parsed.
    ///
    /// # Returns
    ///
    /// * `ChunkMessageHeader` - Returns a new `ChunkMessageHeader` instance with the parsed values.
    pub(crate) fn type1(bytes: &[u8]) -> ChunkMessageHeader {
        let mut chunk_message_header = ChunkMessageHeader::default();

        let timestamp_delta = u32::from_be_bytes([0, bytes[0], bytes[1], bytes[2]]);
        let message_length = u32::from_be_bytes([0, bytes[3], bytes[4], bytes[5]]);
        let message_type_id = bytes[6];

        chunk_message_header.timestamp_delta = Some(timestamp_delta);
        chunk_message_header.message_length = Some(message_length);
        chunk_message_header.message_type_id = Some(message_type_id);

        chunk_message_header
    }

    /// Constructs a new `ChunkMessageHeader` instance from a byte slice representing a type 2 chunk.
    ///
    /// # Arguments
    ///
    /// * `bytes` - A byte slice that represents the data to be parsed.
    ///
    /// # Returns
    ///
    /// * `ChunkMessageHeader` - Returns a new `ChunkMessageHeader` instance with the parsed values.
    pub(crate) fn type2(bytes: &[u8]) -> ChunkMessageHeader {
        let mut chunk_message_header = ChunkMessageHeader::default();

        chunk_message_header.timestamp_delta =
            Some(u32::from_be_bytes([0, bytes[0], bytes[1], bytes[2]]));

        chunk_message_header
    }

    /// Constructs a new `ChunkMessageHeader` instance with default values for a type 3 chunk.
    ///
    /// # Returns
    ///
    /// * `ChunkMessageHeader` - Returns a new `ChunkMessageHeader` instance with all fields set to `None`.
    pub(crate) fn type3() -> ChunkMessageHeader {
        ChunkMessageHeader::default()
    }
}