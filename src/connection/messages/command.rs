use bytes::BytesMut;
use bytesio::bytes_reader::BytesReader;
use log::debug;
use crate::amf0::define::Amf0ValueType;
use crate::amf0::reader::Amf0Reader;
use crate::connection::messages::connect::ConnectMessage;
use crate::connection::messages::RtmpCommand;

#[derive(Debug, Default)]
pub struct Command {
    pub name: String,
    pub transaction_id: usize,
    pub amf0_null: Amf0ValueType,
    pub stream_key: Option<String>,
    pub stream_id: Option<usize>,
}

impl Command {
    pub fn parse(data: &[u8]) -> Result<RtmpCommand, Box<dyn std::error::Error>> {
        let mut reader = Amf0Reader::new(BytesReader::new(BytesMut::from(data.clone())));

        let command_data = reader.read_all()?;

        // commands are wierd and dont follow a strict format and can have diffrent values or number of values
        // so we have to check for each command after a certain point
        let command_name = match command_data.get(0) {
            Some(Amf0ValueType::UTF8String(command_name)) => command_name.to_owned(),
            _ => {
                return Err(Box::new(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Expected command name",
                )))
            }
        };
        #[cfg(debug_assertions)]
        debug!("command_name: {}", command_name);

        // must be a better way, but it makes my brain hurt to think about it atm
        if command_name == "connect" {
            // create a connect command
            let command = ConnectMessage::parse(&data)?;
            return Ok(RtmpCommand::Connect(command));
        }

        let transaction_id = match command_data.get(1) {
            Some(Amf0ValueType::Number(transaction_id)) => *transaction_id as usize,
            _ => {
                return Err(Box::new(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Expected transaction id",
                )))
            }
        };
        #[cfg(debug_assertions)]
        debug!("transaction_id: {}", transaction_id);


        let amf0_null = match command_data.get(2) {
            Some(Amf0ValueType::Null) => Amf0ValueType::Null,
            _ => {
                return Err(Box::new(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Expected null",
                )))
            }
        };

        #[cfg(debug_assertions)]
        debug!("amf0_null: {:?}", amf0_null);

        // the fourth value is present in some commands
        let stream_key = if command_data.len() >= 4 {
            match command_data.get(3) {
                Some(Amf0ValueType::UTF8String(stream_key)) => Some(stream_key.to_owned()),
                _ => {
                    return Err(Box::new(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        "Expected stream key",
                    )))
                }
            }
        } else {
            None
        };

        #[cfg(debug_assertions)]
        debug!("stream_key: {:?}", stream_key);

        // the fifth value is present in some commands
        let stream_id = if command_data.len() >= 5 {
            match command_data.get(4) {
                Some(Amf0ValueType::Number(stream_id)) => Some(*stream_id as usize),
                _ => {
                    return Err(Box::new(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        "Expected stream id",
                    )))
                }
            }
        } else {
            None
        };

        #[cfg(debug_assertions)]
        debug!("stream_id: {:?}", stream_id);

        return match command_name.as_str() {
            "releaseStream" => {
                Ok(RtmpCommand::RelseaedStream(Command {
                    name: command_name,
                    transaction_id,
                    amf0_null,
                    stream_key,
                    stream_id,
                }))
            }
            "FCPublish" => {
                Ok(RtmpCommand::FCPublish(Command {
                    name: command_name,
                    transaction_id,
                    amf0_null,
                    stream_key,
                    stream_id,
                }))
            }
            "createStream" => {
                Ok(RtmpCommand::CreateStream(Command {
                    name: command_name,
                    transaction_id,
                    amf0_null,
                    stream_key,
                    stream_id,
                }))
            }
            "publish" => {
                Ok(RtmpCommand::Publish(Command {
                    name: command_name,
                    transaction_id,
                    amf0_null,
                    stream_key,
                    stream_id,
                }))
            }
            _ => {
                Err(Box::new(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Unknown command",
                )))
            }
        }
    }
}