
use log::error;
use crate::connection::messages::define::ChunkBasicHeader;

/// This function reads the basic header from the provided data.
///
/// # Arguments
///
/// * `data` - A byte slice that holds the data from which the basic header is to be read.
/// * `marker` - A reference to the index in the data where the basic header starts.
///
/// # Returns
///
/// * `Ok(ChunkBasicHeader)` - If the basic header is successfully read, it returns a `ChunkBasicHeader` object.
/// * `Err(Box<dyn std::error::Error>)` - If the data is empty, it logs an error message and returns an error.
pub fn read_basic_header(
    data: &[u8],
    marker: &usize,
) -> Result<ChunkBasicHeader, Box<dyn std::error::Error>> {
    if data.is_empty() {
        error!("No message to read");
        return Err("No message to read".into());
    }

    Ok(ChunkBasicHeader::new(&data[*marker]))
}
