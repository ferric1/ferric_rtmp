use bytes::BytesMut;
use bytesio::bytes_reader::BytesReader;
use indexmap::IndexMap;
use log::error;
use crate::amf0::define::Amf0ValueType;
use crate::amf0::reader::Amf0Reader;

#[derive(Debug)]
pub struct SetDataFrame {
    data_name: String,
    metadata: String,
    data: SetDataFrameData,
}

impl SetDataFrame {
    pub fn new(data_name: String, metadata: String, data: SetDataFrameData) -> SetDataFrame {
        SetDataFrame {
            data_name,
            metadata,
            data,
        }
    }

    pub fn parse(data: &[u8]) -> Result<SetDataFrame, Box<dyn std::error::Error>> {
        let mut reader = Amf0Reader::new(BytesReader::new(BytesMut::from(data)));
        let decoded_msg = reader.read_all()?;
        let data_name = match decoded_msg.get(0) {
            Some(Amf0ValueType::UTF8String(data_name)) => data_name.to_owned(),
            _ => {
                return Err(Box::new(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Expected data name",
                )))
            }
        };
        let metadata = match decoded_msg.get(1) {
            Some(Amf0ValueType::UTF8String(metadata)) => metadata.to_owned(),
            _ => {
                return Err(Box::new(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Expected metadata",
                )))
            }
        };
        let data_obj = match decoded_msg.get(2) {
            Some(Amf0ValueType::Object(data_obj)) => data_obj.to_owned(),
            _ => {
                return Err(Box::new(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    "Expected data",
                )))
            }
        };

        Ok(SetDataFrame::new(
            data_name,
            metadata,
            SetDataFrameData::parse(data_obj).unwrap(),
        ))
    }
}

#[derive(Debug)]
pub struct SetDataFrameData {
    pub duration: f64,
    pub file_size: f64,
    pub width: f64,
    pub height: f64,
    pub video_codec_id: f64,
    pub video_data_rate: f64,
    pub frame_rate: f64,
    pub audio_codec_id: f64,
    pub audio_data_rate: f64,
    pub audio_sample_rate: f64,
    pub audio_sample_size: f64,
    pub audio_channels: f64,
    pub stereo: bool,
    pub two_point_one: bool,
    pub three_point_one: bool,
    pub four_point_zero: bool,
    pub four_point_one: bool,
    pub five_point_one: bool,
    pub seven_point_one: bool,
    pub encoder: String,
}

impl SetDataFrameData {
    pub fn default() -> SetDataFrameData {
        SetDataFrameData {
            duration: 0.0,
            file_size: 0.0,
            width: 0.0,
            height: 0.0,
            video_codec_id: 0.0,
            video_data_rate: 0.0,
            frame_rate: 0.0,
            audio_codec_id: 0.0,
            audio_data_rate: 0.0,
            audio_sample_rate: 0.0,
            audio_sample_size: 0.0,
            audio_channels: 0.0,
            stereo: false,
            two_point_one: false,
            three_point_one: false,
            four_point_zero: false,
            four_point_one: false,
            five_point_one: false,
            seven_point_one: false,
            encoder: "".to_owned(),
        }
    }
    pub fn parse(
        data: IndexMap<String, Amf0ValueType>,
    ) -> Result<SetDataFrameData, Box<dyn std::error::Error>> {
        let mut set_data_frame_data = SetDataFrameData::default();

        for (key, value) in data {
            match key.as_str() {
                "duration" => match value {
                    Amf0ValueType::Number(duration) => set_data_frame_data.duration = duration,
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "fileSize" => match value {
                    Amf0ValueType::Number(file_size) => set_data_frame_data.file_size = file_size,
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "width" => match value {
                    Amf0ValueType::Number(width) => set_data_frame_data.width = width,
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "height" => match value {
                    Amf0ValueType::Number(height) => set_data_frame_data.height = height,
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "videocodecid" => match value {
                    Amf0ValueType::Number(video_codec_id) => {
                        set_data_frame_data.video_codec_id = video_codec_id
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "videodatarate" => match value {
                    Amf0ValueType::Number(video_data_rate) => {
                        set_data_frame_data.video_data_rate = video_data_rate
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "framerate" => match value {
                    Amf0ValueType::Number(frame_rate) => {
                        set_data_frame_data.frame_rate = frame_rate
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "audiocodecid" => match value {
                    Amf0ValueType::Number(audio_codec_id) => {
                        set_data_frame_data.audio_codec_id = audio_codec_id
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "audiodatarate" => match value {
                    Amf0ValueType::Number(audio_data_rate) => {
                        set_data_frame_data.audio_data_rate = audio_data_rate
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "audiosamplerate" => match value {
                    Amf0ValueType::Number(audio_sample_rate) => {
                        set_data_frame_data.audio_sample_rate = audio_sample_rate
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "audiosamplesize" => match value {
                    Amf0ValueType::Number(audio_sample_size) => {
                        set_data_frame_data.audio_sample_size = audio_sample_size
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "audiochannels" => match value {
                    Amf0ValueType::Number(audio_channels) => {
                        set_data_frame_data.audio_channels = audio_channels
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected number",
                        )))
                    }
                },
                "stereo" => match value {
                    Amf0ValueType::Boolean(stereo) => set_data_frame_data.stereo = stereo,
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected boolean",
                        )))
                    }
                },
                "2.1" => match value {
                    Amf0ValueType::Boolean(two_point_one) => {
                        set_data_frame_data.two_point_one = two_point_one
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected boolean",
                        )))
                    }
                },
                "3.1" => match value {
                    Amf0ValueType::Boolean(three_point_one) => {
                        set_data_frame_data.three_point_one = three_point_one
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected boolean",
                        )))
                    }
                },
                "4.0" => match value {
                    Amf0ValueType::Boolean(four_point_zero) => {
                        set_data_frame_data.four_point_zero = four_point_zero
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected boolean",
                        )))
                    }
                },
                "4.1" => match value {
                    Amf0ValueType::Boolean(four_point_one) => {
                        set_data_frame_data.four_point_one = four_point_one
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected boolean",
                        )))
                    }
                },
                "5.1" => match value {
                    Amf0ValueType::Boolean(five_point_one) => {
                        set_data_frame_data.five_point_one = five_point_one
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected boolean",
                        )))
                    }
                },
                "7.1" => match value {
                    Amf0ValueType::Boolean(seven_point_one) => {
                        set_data_frame_data.seven_point_one = seven_point_one
                    }
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected boolean",
                        )))
                    }
                },
                "encoder" => match value {
                    Amf0ValueType::UTF8String(encoder) => set_data_frame_data.encoder = encoder,
                    _ => {
                        return Err(Box::new(std::io::Error::new(
                            std::io::ErrorKind::InvalidData,
                            "Expected string",
                        )))
                    }
                },
                _ => {
                    error!("Unexpected key {:?}", key);
                    return Err(Box::new(std::io::Error::new(
                        std::io::ErrorKind::InvalidData,
                        "Unexpected key",
                    )));
                }
            }
        }
        Ok(set_data_frame_data)
    }
}