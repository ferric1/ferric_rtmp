use log::{error, info};
use crate::connection::Connection;
use crate::connection::define::msg_type_id;
use crate::connection::messages::acknowledgement_message::AcknowledgementMessage;
use crate::connection::messages::command::Command;
use crate::connection::messages::data_message::DataMessage;
use crate::connection::messages::connect::ConnectMessage;
use crate::connection::messages::define::ChunkMessageHeader;
use crate::connection::messages::reader::{read_ack, read_meassage_name, read_set_chunk};
use crate::connection::messages::RtmpMessage;
use crate::connection::messages::set_chunk_size::SetChunkSizeMessage;
use crate::connection::messages::set_data_frame::SetDataFrame;

pub fn read_msg_type(
    connection: &mut Connection,
    data: &[u8],
    msg_header: ChunkMessageHeader,
) -> Result<RtmpMessage, Box<dyn std::error::Error>> {
    match msg_header.message_type_id {
        Some(msg_type_id::SET_CHUNK_SIZE) => {
            info!("Message type: Set Chunk Size");
            let read_to = connection.marker + 4;
            let chunk_size = read_set_chunk(&data[connection.marker..read_to])?;
            info!("chunk_size: {}", chunk_size);
            connection.marker = read_to;
            let set_chunk_size = SetChunkSizeMessage::new(chunk_size as usize);
            return Ok(RtmpMessage::SetChunkSize(set_chunk_size));
        }
        Some(msg_type_id::ABORT) => {
            info!("Message type: Abort");
        }
        Some(msg_type_id::ACKNOWLEDGEMENT) => {
            info!("Message type: Acknowledgement");
            let read_to = connection.marker + 4;
            let ack_sequence_number = read_ack(&data[connection.marker..read_to])?;
            connection.marker = read_to;
            let ack = AcknowledgementMessage::new(ack_sequence_number as usize);
            info!("ack: {:?}", ack);
            return Ok(RtmpMessage::Acknowledgement(ack));
        }
        Some(msg_type_id::USER_CONTROL_EVENT) => {
            info!("Message type: User Control");
        }
        Some(msg_type_id::WIN_ACKNOWLEDGEMENT_SIZE) => {
            info!("Message type: Window Acknowledgement Size");
        }
        Some(msg_type_id::SET_PEER_BANDWIDTH) => {
            info!("Message type: Set Peer Bandwidth");
        }
        Some(msg_type_id::AUDIO) => {
            info!("Message type: Audio");
            let read_to = connection.marker + msg_header.message_length.unwrap() as usize;
            let audio_data = DataMessage::new(
                msg_header.message_stream_id.unwrap() as usize,
                data[connection.marker..read_to].to_vec(),
            );
            connection.marker = read_to;
            return Ok(RtmpMessage::AudioData(audio_data));
        }
        Some(msg_type_id::VIDEO) => {
            info!("Message type: Video");
            let read_to = connection.marker + msg_header.message_length.unwrap() as usize;
            let video_data = DataMessage::new(
                msg_header.message_stream_id.unwrap() as usize,
                data[connection.marker..read_to].to_vec(),
            );
            connection.marker = read_to;
            return Ok(RtmpMessage::VideoData(video_data));
        }
        Some(msg_type_id::COMMAND_AMF3) => {
            info!("Message type: Command AMF3");
        }
        Some(msg_type_id::DATA_AMF3) => {
            info!("Message type: Data AMF3");
        }
        Some(msg_type_id::SHARED_OBJ_AMF3) => {
            info!("Message type: Shared Object AMF3");
        }
        Some(msg_type_id::DATA_AMF0) => {
            info!("Message type: Data AMF0");
            if let Some(msg_len) = msg_header.message_length {
                let read_to = connection.marker + msg_len as usize;
                let msg_name = read_meassage_name(&data[connection.marker..read_to])?;
                info!("msg_name: {:?}", msg_name);
                return match msg_name.as_str() {
                    "@setDataFrame" => {
                        let message = SetDataFrame::parse(&data[connection.marker..read_to])?;
                        connection.marker = read_to;
                        info!("message: {:?}", message);
                        Ok(RtmpMessage::SetDataFrame(message))
                    }
                    _ => {
                        error!("Unknown Data: {:?}", msg_name);
                        Err("Unknown Data".into())
                    }
                }
            }
        }
        Some(msg_type_id::SHARED_OBJ_AMF0) => {
            info!("Message type: Shared Object AMF0");
        }
        Some(msg_type_id::AGGREGATE) => {
            info!("Message type: Aggregate");
        }
        // todo: make all comands be of one type with options for the non required fields
        Some(msg_type_id::COMMAND_AMF0) => {
            info!("Message type: Command AMF0");
            return if let Some(msg_len) = msg_header.message_length {
                let read_to = connection.marker + msg_len as usize;
                let command = Command::parse(&data[connection.marker..read_to])?;
                connection.marker = read_to;
                Ok(RtmpMessage::Command(command))
            } else {
                let command = Command::parse(&data[connection.marker..])?;
                Ok(RtmpMessage::Command(command))
            }
        }
        _ => {
            error!("Message type: Unknown");
            return Err("Unknown message type".into());
        }
    }
    error!("Unknown message type");
    Err("Unknown message type".into())
}