pub mod read_msg;

use std::usize;
use bytes::BytesMut;
use bytesio::bytes_reader::BytesReader;
use log::{debug, error, info, warn};
use tokio::io::AsyncReadExt;
use crate::amf0::errors::Amf0ReadError;
use crate::amf0::reader::Amf0Reader;
use crate::connection::Connection;
use crate::connection::messages::define::{ChunkBasicHeader, ChunkFmt, ChunkMessageHeader};
use crate::connection::messages::reader::read_msg::read_msg_type;
use crate::connection::messages::RtmpMessage;

impl Connection {
    pub(crate) async fn read_message(&mut self) -> Result<RtmpMessage, Box<dyn std::error::Error>> {
        // Create a buffer to hold the message data.
        let mut buffer = vec![0; 12288]; // Adjust the size as needed.

        // Read data from the client into the buffer.
        let size = self.stream.read(&mut buffer).await?;
        // info!("Read {} bytes", size);
        self.marker = 0;

        // Parse the data into an RtmpMessage.
        let message = parse_message(self, &buffer[0..size])?;

        Ok(message)
    }
}

pub fn parse_message(connection: &mut Connection, data: &[u8]) -> Result<RtmpMessage, Box<dyn std::error::Error>> {
    debug!("Parse Message data: {:?}", data);

    let msg = parse_msg_header(connection, data)?;

    // check for more msg types
    if connection.marker < data.len() && &data[connection.marker] != &0 {
        debug!("more msg types");
        let message = parse_message(connection, data)?;
        return Ok(message);
    }

    Ok(msg)
}

fn parse_msg_header(
    mut connection: &mut Connection,
    data: &[u8],
) -> Result<RtmpMessage, Box<dyn std::error::Error>> {
    if data.is_empty() {
        error!("No message to read");
        return Err("No message to read".into());
    }

    debug!("Marker Before CBH: {}", connection.marker);
    let basic_header = ChunkBasicHeader::new(&data[connection.marker]);
    connection.marker += 1;
    info!("fmt: {:?}, cs: {}", basic_header.fmt, basic_header.cs);
    let msg = read_header_types(connection, data, basic_header)?;
    Ok(msg)
}

fn read_header_types(
    mut connection: &mut Connection,
    data: &[u8],
    header: ChunkBasicHeader,
) -> Result<RtmpMessage, Box<dyn std::error::Error>> {
    return match header.fmt {
        ChunkFmt::Type0 => {
            let read_to = connection.marker + 11;
            let chunk_message_header = ChunkMessageHeader::type0(&data[connection.marker..read_to]);
            connection.marker = read_to;
            let msg = read_msg_type(connection, data, chunk_message_header);
            msg
        }
        ChunkFmt::Type1 => {
            let read_to = connection.marker + 7;
            let chunk_message_header = ChunkMessageHeader::type1(&data[connection.marker..read_to]);
            connection.marker = read_to;
            let msg = read_msg_type(connection, data, chunk_message_header);
            msg
        }
        ChunkFmt::Type2 => {
            let read_to = connection.marker + 3;
            let chunk_message_header = ChunkMessageHeader::type2(&data[connection.marker..read_to]);
            connection.marker = read_to;
            let msg = read_msg_type(connection, data, chunk_message_header);
            msg
        }
        ChunkFmt::Type3 => {
            let chunk_message_header = ChunkMessageHeader::type3();
            let msg = read_msg_type(connection, data, chunk_message_header);
            msg
        }
    }
}

pub fn read_meassage_name(data: &[u8]) -> Result<String, Amf0ReadError> {
    let mut reader = Amf0Reader::new(BytesReader::new(BytesMut::from(data)));

    reader.read_raw_string()
}

fn read_set_chunk(data: &[u8]) -> Result<u32, Box<dyn std::error::Error>> {
    let tmp_data = (data[0] << 1) >> 1;
    let chunk_size = u32::from_be_bytes([tmp_data, data[1], data[2], data[3]]);
    Ok(chunk_size)
}

fn read_ack(data: &[u8]) -> Result<u32, Box<dyn std::error::Error>> {
    let tmp_data = (data[0] << 1) >> 1;
    let ack = u32::from_be_bytes([tmp_data, data[1], data[2], data[3]]);
    Ok(ack)
}