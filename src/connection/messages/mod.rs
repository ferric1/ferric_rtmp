use bytes::BytesMut;
use crate::amf0::errors::Amf0WriteError;
use crate::connection::messages::command::Command;

pub(crate) mod connect;
pub mod reader;
pub mod cbh_reader;
pub mod define;
pub mod set_chunk_size;
pub mod result;
pub mod acknowledgement_message;
pub mod data_message;
pub mod command;
mod set_data_frame;


/// The `RtmpMessage` enum represents the types of RTMP messages that can be handled by the application.
///
/// # Variants
///
/// * `ConnectMessage` - Represents a connection message.
#[derive(Debug)]
pub enum RtmpMessage {
    SetChunkSize(set_chunk_size::SetChunkSizeMessage),
    Acknowledgement(acknowledgement_message::AcknowledgementMessage),
    AudioData(data_message::DataMessage),
    VideoData(data_message::DataMessage),
    SetDataFrame(set_data_frame::SetDataFrame),

    Command(RtmpCommand),
}

#[derive(Debug)]
pub enum RtmpCommand {
    Connect(connect::ConnectMessage),
    RelseaedStream(Command),
    FCPublish(Command),
    CreateStream(Command),
    Publish(Command),
}

/// The `CommandObject` struct represents a command object in the application.
///
/// # Fields
///
/// * `fms_ver` - A string that represents the version of the Flash Media Server used by the client.
/// * `capabilities` - A number that represents the capabilities of the client.
pub struct CommandObject {
    fms_ver: String,
    capabilities: usize,
}

impl CommandObject {
    /// Constructs a new `CommandObject` instance.
    ///
    /// # Arguments
    ///
    /// * `fms_ver` - A string that represents the version of the Flash Media Server used by the client.
    /// * `capabilities` - A number that represents the capabilities of the client.
    ///
    /// # Returns
    ///
    /// * `CommandObject` - Returns a new `CommandObject` instance with the specified parameters.
    pub fn new(fms_ver: String, capabilities: usize) -> CommandObject {
        CommandObject {
            fms_ver,
            capabilities,
        }
    }
}
