#[allow(dead_code)]

/// This module defines the message type IDs used in the RTMP protocol.
///
/// # Constants
///
/// * `AUDIO` - Represents the ID for audio messages.
/// * `VIDEO` - Represents the ID for video messages.
///
/// * `SET_CHUNK_SIZE` - Represents the ID for set chunk size messages.
/// * `ABORT` - Represents the ID for abort messages.
/// * `ACKNOWLEDGEMENT` - Represents the ID for acknowledgement messages.
/// * `USER_CONTROL_EVENT` - Represents the ID for user control event messages.
/// * `WIN_ACKNOWLEDGEMENT_SIZE` - Represents the ID for window acknowledgement size messages.
/// * `SET_PEER_BANDWIDTH` - Represents the ID for set peer bandwidth messages.
///
/// * `COMMAND_AMF3` - Represents the ID for AMF3 command messages.
/// * `COMMAND_AMF0` - Represents the ID for AMF0 command messages.
///
/// * `DATA_AMF3` - Represents the ID for AMF3 data messages.
/// * `DATA_AMF0` - Represents the ID for AMF0 data messages.
///
/// * `SHARED_OBJ_AMF3` - Represents the ID for AMF3 shared object messages.
/// * `SHARED_OBJ_AMF0` - Represents the ID for AMF0 shared object messages.
///
/// * `AGGREGATE` - Represents the ID for aggregate messages.
pub mod msg_type_id {
    pub const AUDIO: u8 = 8;
    pub const VIDEO: u8 = 9;

    pub const SET_CHUNK_SIZE: u8 = 1;
    pub const ABORT: u8 = 2;
    pub const ACKNOWLEDGEMENT: u8 = 3;
    pub const USER_CONTROL_EVENT: u8 = 4;
    pub const WIN_ACKNOWLEDGEMENT_SIZE: u8 = 5;
    pub const SET_PEER_BANDWIDTH: u8 = 6;

    pub const COMMAND_AMF3: u8 = 17;
    pub const COMMAND_AMF0: u8 = 20;

    pub const DATA_AMF3: u8 = 15;
    pub const DATA_AMF0: u8 = 18;

    pub const SHARED_OBJ_AMF3: u8 = 16;
    pub const SHARED_OBJ_AMF0: u8 = 19;

    pub const AGGREGATE: u8 = 22;
}

/// This module defines the default values used in the connection.
///
/// # Constants
///
/// * `WINDOW_ACKNOWLEDGEMENT_SIZE` - Represents the default window acknowledgement size for the connection.
/// * `PEER_BANDWIDTH_SIZE` - Represents the default peer bandwidth size for the connection.
/// * `CHUNK_SIZE` - Represents the default chunk size for the connection.
pub mod connection_defaults {
    pub const WINDOW_ACKNOWLEDGEMENT_SIZE: u32 = 4096;
    pub const PEER_BANDWIDTH_SIZE: u32 = 4096;
    pub const CHUNK_SIZE: u32 = 128;
}

/// The `ConnectionState` enum represents the various states a connection can be in.
///
/// # Variants
///
/// * `Uninitialized` - The connection has been created but not yet initialized.
/// * `Handshake` - The connection is in the process of performing a handshake.
/// * `Conecting` - The connection is in the process of connecting.
/// * `Connected` - The connection has been successfully established.
/// * `Closing` - The connection is in the process of closing.
/// * `Closed` - The connection has been closed.
#[derive(Debug)]
pub(crate) enum ConnectionState {
    Uninitialized,
    Handshake,
    Conecting,
    Connected,
    Closing,
    Closed,
}
