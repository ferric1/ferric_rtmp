use std::sync::{Arc, Mutex};
use crate::connection::define::connection_defaults::{
    CHUNK_SIZE, PEER_BANDWIDTH_SIZE, WINDOW_ACKNOWLEDGEMENT_SIZE,
};
use crate::connection::define::ConnectionState;
use crate::connection::rtmp_functions::handshake::handshake;
use crate::connection::{Connection, ObservableConnectionState};
use log::{error, info};
use rand::{Rng, SeedableRng};
use tokio::net::TcpStream;

// Impl for Connection struct
impl Connection {
    /// Constructs a new `Connection` instance.
    ///
    /// # Arguments
    ///
    /// * `stream` - A `TcpStream` instance that represents the new connection.
    ///
    /// # Functionality
    ///
    /// This function does the following:
    /// 1. Generates a random unique identifier for the connection.
    /// 2. Initializes the connection with the specified stream, the unique identifier, default chunk size, window acknowledggment size, peer bandwidth size, a marker set to 0, and the state set to `Uninitialized`.
    ///
    /// # Returns
    ///
    /// * `Self` - Returns a new `Connection` instance with the specified parameters.
    pub fn new(stream: TcpStream) -> Connection {
        let mut rng = rand::rngs::StdRng::from_entropy();
        let id: u64 = rng.gen();
        let observable_state = ObservableConnectionState {
            id,
            chunk_size: CHUNK_SIZE,
            window_ack_size: WINDOW_ACKNOWLEDGEMENT_SIZE,
            peer_bandwidth: PEER_BANDWIDTH_SIZE,
        };
        Self {
            stream,
            marker: 0,
            state: ConnectionState::Uninitialized,
            observable_state: Arc::new(Mutex::new(observable_state)),
        }
    }

    /// This function runs the connection.
    ///
    /// # Functionality
    ///
    /// This function does the following:
    /// 1. Logs the start of the connection.
    /// 2. Calls the `start_connection` method to start the connection.
    /// 3. Enters a loop where it checks the state of the connection.
    /// 4. If the connection is in the `Conecting` state, it logs the state and continues the loop.
    /// 5. If the connection is not in the `Conecting` state, it logs an error message and returns an error.
    ///
    /// # Returns
    ///
    /// * `Ok(())` - If the connection runs successfully, it returns an empty Ok result.
    /// * `Err(Box<dyn std::error::Error>)` - If any part of the connection run fails, it returns an error.
    pub async fn run(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        {
            info!("Starting connection {}", self.observable_state.lock().unwrap().id);
        }

        match self.start_connection().await {
            Ok(_) => (),
            Err(e) => {
                error!("Failed to start connection: {}", e);
                return Err(e);
            }
        }

        loop {
            match self.state {
                ConnectionState::Conecting => {
                    info!("Connecting");
                    self.handle_connect().await;
                    self.state = ConnectionState::Connected;
                }
                _ => {
                    error!("Failed to connect");
                    return Err("Connection is not in connecting state".into());
                }
            }
        }
    }

    /// This function starts the connection.
    ///
    /// # Functionality
    ///
    /// This function does the following:
    /// 1. Checks the current state of the connection.
    /// 2. If the connection is `Uninitialized`, it changes the state to `Handshake` and performs a handshake.
    /// 3. If the handshake is successful, it changes the state to `Conecting`.
    /// 4. If the handshake fails, it logs an error message and returns an error.
    /// 5. If the connection is not `Uninitialized`, it logs an error message and returns an error.
    ///
    /// # Returns
    ///
    /// * `Ok(())` - If the connection starts successfully, it returns an empty Ok result.
    /// * `Err(Box<dyn std::error::Error>)` - If any part of the connection start fails, it returns an error.
    async fn start_connection(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        match self.state {
            ConnectionState::Uninitialized => {
                self.state = ConnectionState::Handshake;
                if let Err(e) = handshake(&mut self.stream).await {
                    error!("Failed to handshake: {}", e);
                    return Err(e);
                } else {
                    info!("Handshake successful");
                    self.state = ConnectionState::Conecting;
                }
            }
            _ => {
                error!("Failed to start connection: connection is already initialized");
                return Err("Connection is already initialized".into());
            }
        }
        Ok(())
    }
}

impl ObservableConnectionState {
    /// Constructs a new `ObservableConnectionState` instance.
    ///
    /// # Arguments
    ///
    /// * `id` - A u64 that represents the unique identifier for the connection.
    /// * `chunk_size` - A u32 that represents the chunk size for the connection.
    /// * `window_ack_size` - A u32 that represents the window acknowledgement size for the connection.
    /// * `peer_bandwidth` - A u32 that represents the peer bandwidth for the connection.
    ///
    /// # Functionality
    ///
    /// This function does the following:
    /// 1. Initializes the connection with the specified unique identifier, chunk size, window acknowledgement size, and peer bandwidth.
    ///
    /// # Returns
    ///
    /// * `Self` - Returns a new `ObservableConnectionState` instance with the specified parameters.
    pub fn new(id: u64, chunk_size: u32, window_ack_size: u32, peer_bandwidth: u32) -> Self {
        Self {
            id,
            chunk_size,
            window_ack_size,
            peer_bandwidth,
        }
    }
}
