use log::error;
use rand::{Rng, SeedableRng};
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::TcpStream;

/// This function performs the RTMP handshake with the client.
///
/// # Arguments
///
/// * `stream` - A mutable reference to the TcpStream object representing the client connection.
///
/// # Returns
///
/// * `Ok(())` - If the handshake is successful, it returns an empty Ok result.
/// * `Err(Box<dyn std::error::Error>)` - If any part of the handshake fails, it logs an error message and returns an error.
pub(crate) async fn handshake(stream: &mut TcpStream) -> Result<(), Box<dyn std::error::Error>> {
    let mut c0_c1_buffer = [0; 1537]; // Size for C0 + C1
    let mut s0_s1_s2_buffer = [0; 3073]; // Size for S0 + S1 + S2
    let mut c2_buffer = [0; 1536]; // Size for C2

    // Read C0 and C1 from the client.
    stream.read_exact(&mut c0_c1_buffer).await?;

    // Check the RTMP version in C0.
    let version = c0_c1_buffer[0];
    if version != 3 {
        error!("Unsupported RTMP version: {}", version);
        return Err("Unsupported RTMP version".into());
    }

    // Check the timestamp in C1.
    let _timestamp = u32::from_be_bytes([
        c0_c1_buffer[1],
        c0_c1_buffer[2],
        c0_c1_buffer[3],
        c0_c1_buffer[4],
    ]);

    // Construct S0.
    s0_s1_s2_buffer[0] = 3; // RTMP version

    // Construct S1.
    let server_timestamp = (0u32).to_be_bytes(); // Server uptime in milliseconds
    s0_s1_s2_buffer[1..5].copy_from_slice(&server_timestamp);

    let mut rng = rand::rngs::StdRng::from_entropy();
    rng.fill(&mut s0_s1_s2_buffer[5..3073]);
    let s1_clone;
    {
        let s1 = &s0_s1_s2_buffer[1..1537];
        s1_clone = s1.to_vec(); // Clone s1 into s1_clone
    }
    // Construct S2 by copying C1.
    let c1 = &c0_c1_buffer[1..1537];
    s0_s1_s2_buffer[1537..3073].copy_from_slice(c1);

    // Write S0, S1, and S2 to the client.
    stream.write_all(&s0_s1_s2_buffer).await?;

    // Read C2 from the client.
    stream.read_exact(&mut c2_buffer).await?;

    // Check that C2 matches S1.
    if c2_buffer != s1_clone.as_slice() {
        error!("C2 does not match S1");
        return Err("C2 does not match S1".into());
    }

    Ok(())
}
