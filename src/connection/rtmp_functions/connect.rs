use log::info;
use tokio::io::AsyncWriteExt;
use crate::connection::messages::connect::ConnectMessage;
use crate::connection::Connection;
use crate::connection::define::connection_defaults::{PEER_BANDWIDTH_SIZE, WINDOW_ACKNOWLEDGEMENT_SIZE};
use crate::connection::define::msg_type_id::SET_PEER_BANDWIDTH;
use crate::connection::messages::{CommandObject, result::ResultObject};
use crate::connection::rtmp_functions::write_header::write_header;

impl Connection {
    /// Handles the connection process for a given RTMP connection.
    ///
    /// # Arguments
    ///
    /// * `connection` - A mutable reference to the `Connection` struct representing the RTMP connection.
    /// * `msg` - A `ConnectMessage` struct representing the connection message.
    ///
    /// # Functionality
    ///
    /// This function does the following:
    /// 1. Logs the start of the connection process.
    /// 2. Logs the connection message.
    /// 3. Creates and sends an acknowledgement message to the client.
    /// 4. Creates and sends a bandwidth message to the client.
    /// 5. Creates a command object with the specified FMS version and capabilities.
    /// 6. Creates a result object with the specified command name, transaction ID, and stream ID.
    /// 7. Sets the command object of the result object.
    /// 8. Parses the result object into a byte sequence.
    /// 9. Freezes the command into an immutable byte sequence and converts it to a vector.
    /// 10. Creates and sends a SET_PEER_BANDWIDTH message to the client.
    ///
    /// # Returns
    ///
    /// * `Ok(())` - If the connection process is successful, it returns an empty Ok result.
    /// * `Err(Box<dyn std::error::Error>)` - If any part of the connection process fails, it returns an error.
    pub async fn connect(&mut self, msg: ConnectMessage) -> Result<(), Box<dyn std::error::Error>> {
        info!("==========Start Connect msg Handle==========");
        info!("Connect message: {:?}", msg);

        let ack_header = write_header(5, 4, 0, 0, 2);
        let mut ack_msg: [u8; 16] = [0; 16];

        ack_msg[0..12].copy_from_slice(&ack_header);
        ack_msg[12..16].copy_from_slice(&WINDOW_ACKNOWLEDGEMENT_SIZE.to_be_bytes());
        self.stream.write_all(&ack_msg).await?;

        let bandwidth_header = write_header(6, 5, 0, 0, 2);
        let mut bandwidth_msg: [u8; 17] = [0; 17];

        bandwidth_msg[0..12].copy_from_slice(&bandwidth_header);

        let bandwidth_as_bytes = PEER_BANDWIDTH_SIZE.to_be_bytes();

        bandwidth_msg[12..16].copy_from_slice(&bandwidth_as_bytes);
        bandwidth_msg[16] = 2;
        let e = CommandObject::new("FMS/3,0,1,123".to_string(), 31);
        let mut result_obj = ResultObject::new("_result".to_string(), 1, 0);
        result_obj.set_command_object(e);
        let command = result_obj.parse()?;
        let command_vec: Vec<u8> = command.freeze().to_vec();

        let command_header = write_header(20, command_vec.len() as u32, 0, 0, 2);
        let mut command_msg = Vec::new();
        command_msg.extend_from_slice(&command_header);
        command_msg.extend_from_slice(&command_vec);

        let mut set_peer_bandwidth = Vec::new();
        set_peer_bandwidth.extend_from_slice(&bandwidth_msg);
        set_peer_bandwidth.extend_from_slice(&command_msg);
        info!("set peer bandwidth: {:?}", set_peer_bandwidth);
        self.stream.write_all(&set_peer_bandwidth).await?;

        self.read_message().await?;

        info!("==========End Connect msg Handle==========");
        Ok(())
    }
}
