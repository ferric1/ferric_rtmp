use log::info;

/// Writes a header for an RTMP message.
///
/// # Arguments
///
/// * `msg_type_id` - The type ID of the RTMP message.
/// * `msg_len` - The length of the RTMP message.
/// * `timestamp` - The timestamp of the RTMP message.
/// * `stream_id` - The ID of the stream that the RTMP message belongs to.
/// * `chunk_basic_header` - The basic header of the RTMP chunk.
///
/// # Returns
///
/// * `[u8; 12]` - Returns a 12-byte array representing the RTMP message header.
pub fn write_header(
    msg_type_id: u8,
    msg_len: u32,
    timestamp: u32,
    stream_id: u32,
    chunk_basic_header: u8,
) -> [u8; 12] {
    /// Inserts a 32-bit integer into a byte array.
    ///
    /// # Arguments
    ///
    /// * `dst` - The destination byte array.
    /// * `data` - The 32-bit integer to be inserted.
    /// * `start_idx` - The start index in the byte array where the data should be inserted.
    /// * `end_idx` - The end index in the byte array where the data should be inserted.
    pub fn insert_bytes(dst: &mut [u8; 12], data: u32, start_idx: usize, end_idx: usize) {
        let data_as_bytes = data.to_be_bytes();

        let mut i: usize = 0;
        if end_idx - start_idx == 3 {
            i = 1;
        }

        for idx in start_idx..end_idx {
            dst[idx] = data_as_bytes[i];
            i += 1;
        }
    }

    let mut header = [0; 12];
    header[0] = chunk_basic_header;
    insert_bytes(&mut header, timestamp, 1, 4);
    insert_bytes(&mut header, msg_len, 4, 7);
    header[7] = msg_type_id;
    insert_bytes(&mut header, stream_id, 8, 12);

    info!("header: {:?}", header);
    header
}