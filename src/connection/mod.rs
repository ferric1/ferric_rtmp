use std::sync::{Arc, Mutex};
use crate::connection::define::ConnectionState;
use tokio::net::TcpStream;

pub mod define;
pub mod impl_connection;
pub mod messages;
pub mod rtmp_functions;
mod handle_connect;

/// The `Connection` struct represents a connection in the application.
///
/// # Fields
///
/// * `id` - A unique identifier for the connection.
/// * `stream` - A `TcpStream` object that represents the TCP connection.
/// * `chunk_size` - The current chunk size for the connection.
/// * `window_ack_size` - The current window acknowledgement size for the connection.
/// * `peer_bandwidth` - The current peer bandwidth for the connection.
/// * `marker` - A marker used for tracking the position in the data stream.
/// * `state` - The current state of the connection, represented by the `ConnectionState` enum.
#[derive(Debug)]
pub struct Connection {
    stream: TcpStream,
    marker: usize,
    state: ConnectionState,

    // Shared observable state
    pub observable_state: Arc<Mutex<ObservableConnectionState>>,
}

#[derive(Debug, Clone, Copy)]
pub struct ObservableConnectionState {
    // Observable attributes
    pub id: u64,
    pub chunk_size: u32,
    pub window_ack_size: u32,
    pub peer_bandwidth: u32,
    // Add other observable attributes here
}
