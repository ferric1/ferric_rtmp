use log::{error, info};
use crate::connection::Connection;
use crate::connection::messages::{RtmpCommand, RtmpMessage};

impl Connection {
    pub async fn handle_connect(&mut self) {
        // parse the message and expect a connect message
        let connect_msg = match self.read_message().await {
            Ok(msg) => msg,
            Err(e) => {
                error!("Failed to parse message: {}", e);
                return;
            }
        };

        match connect_msg {
            RtmpMessage::Command(RtmpCommand::Connect(msg)) => {
                info!("Connect msg: {:?}", msg);

                match self.connect(msg).await {
                    Ok(_) => (),
                    Err(e) => {
                        error!("Failed to connect: {}", e);
                        return;
                    }
                }
            }
            _ => {}
        }

    }
}